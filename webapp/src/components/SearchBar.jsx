import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {Button, Dropdown, FormControl, FormGroup, ControlLabel, Col, Grid, Row,} from 'react-bootstrap';
import './SearchBar.css';
var axios = require('axios');


export default class SearchBar extends Component{
    constructor(props){
        super(props)
        this.state = {searchInput:''}
        this.handleSubmit = (event) => {
                event.preventDefault();
                console.log('Event: Form Submit', this.state.searchInput);
                //nanti disini bakal ngambil data yg udah dibikin sama backend
                axios.get(`https://api.github.com/users/${this.state.searchInput}`).then(resp => {this.props.onSubmit(resp.data);this.setState({searchInput:''})});
            } 
        
    }
    
    render(){
        return(
            <form onSubmit={this.handleSubmit}>
                <Grid>
                    <Row>
                        <Col md={4} sm={4} id='dropdown'>
                            <FormGroup controlId="formControlsSelect">
                                <FormControl componentClass="select" placeholder="Pilih kategori">
                                    <option value="nama">Nama Bengkel</option>
                                    <option value="lokasi">Lokasi Bengkel</option>
                                    <option value="kerusakan">Kebutuhan</option>
                                </FormControl>
                            </FormGroup>
                        </Col>
                        <Col md={4} sm={4}id='searchInput'>
                            <FormGroup>
                                <FormControl type="text" value={this.state.searchInput} onChange={(event)=>this.setState({searchInput:event.target.value})} placeholder="Cari bengkel..." required/>
                            </FormGroup>
                        </Col>
                        <Col md={4} sm={4} id='button'>
                        <Button type="submit">
                        Cari
                        </Button>       
                        </Col>
                    </Row>
                </Grid>
            </form>
        );
    }
}
